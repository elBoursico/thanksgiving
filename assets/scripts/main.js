let app;
const assetsLoader = new AssetsLoader();

function mainLoop() {
    requestAnimationFrame(mainLoop);
    if (assetsLoader.state == "loaded" && !app) {
        console.log("here");
        app = new App(assetsLoader.lib);
    } else if (assetsLoader.state == "loaded" && app) {
        app.update();
    }
}

// var contexteAudio = new AudioContext();

// console.log(contexteAudio);

mainLoop.call(this);
