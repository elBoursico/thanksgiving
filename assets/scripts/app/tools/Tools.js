class Tools {

    constructor() {

    }

    static degreesToRadians(degrees) {

        return degrees * (Math.PI/180);
    }

    static radiansToDegrees(radians) {

        return radians * (180/Math.PI);

    }

    static getRange (min, max) {

        return Math.random() * (max - min) + min;

    }


}
