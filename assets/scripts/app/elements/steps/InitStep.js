class InitStep extends Step {

    constructor(lib) {
        super(lib);
        const book = this.createMesh("livre.js", {
            x : 0.3,
            y : 0.3,
            z : 0.3
        }, false);

        const testText = this.createTextMesh("helvetiker_regular.typeface.json");
        testText.position.set(35, 0, 0);

        const infiniteFloor = new THREE.Mesh(
            new THREE.CircleGeometry(10000,10),
            this.lib.mats["standard"]
        )
        infiniteFloor.position.set(0, -10, 0);
        infiniteFloor.rotation.x = -Math.PI * .5;

        book.castShadow = true;
        book.receiveShadow = true;
        infiniteFloor.receiveShadow = true;

        this.add(book);
        this.add(testText);
        this.add(infiniteFloor);
    }

}
