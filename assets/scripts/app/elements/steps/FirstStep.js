class FirstStep extends Step {

    constructor(lib) {
        super(lib);
        this.sea = this.createTheSea(5, 5);
        this.add(this.sea);
        this.clock = new THREE.Clock();
        this.time = 0;
        this.layDownWave();
    }

    createTheSea(waveNumber, decalageNumber) {

        let group = new THREE.Object3D();

        for (let i = 0; i < waveNumber; i++) {
            let random = Tools.getRange(0,1);
            waveNumber[i]
            let wave = this.createWave();
            if (random > 0.5) {
                wave.position.set(i * -decalageNumber,- 2, i * decalageNumber);
            } else {
                wave.position.set(i * decalageNumber,- 2, i * decalageNumber);
            }
            group.add(wave);
        }

        return group;

    }

    createWave() {

        const group = new THREE.Object3D();
        const part1Wave = this.createMesh("testWave.json", {
                        x : 20,
                        y : 10,
                        z : 10
                    }, true);
        part1Wave.name = "part1Wave";
        part1Wave.castShadow = true;
        part1Wave.receiveShadow = false;
        const part2Wave = part1Wave.clone();
        part2Wave.name = "part2Wave";

        group.add(part1Wave);
        group.add(part2Wave);
        group.partWidth = part1Wave.geometry.boundingBox.max.x + -part1Wave.geometry.boundingBox.min.x;
        part2Wave.position.set(group.partWidth, 0, 0);
        group.randomFactor = Tools.getRange(1,5);
        group.name = "wave";
        return group;
    }

    waveLoopAnimation(wave) {
        this.time += this.clock.getDelta() * 10;

        let cos = (Math.cos(this.time / (wave.randomFactor * 2) ) + 1) / 2;

        for (let i = 0; i < wave.children.length; i++) {

            let wavePart = wave.children[i];

            wavePart.position.x -= 0.2;
            wavePart.position.y = cos * 2;

            if (wavePart.position.x <= -wave.partWidth) {
                console.log("complete turn");
                wavePart.position.set(wave.partWidth,0,0);
            }
        }
    }

    seaLoopAnimation(sea) {
        for (var i = 0; i < sea.children.length; i++) {
            this.waveLoopAnimation(sea.children[i]);
        }
    }

    layDownWave() {
        for (var i = 0; i < this.sea.children.length; i++) {
            for (var j = 0; j < this.sea.children[i].children.length; j++) {
                this.sea.children[i].children[j].rotation.x = Tools.degreesToRadians(-90);
            }
        }
    }

    waveRise() {
        for (var i = 0; i < this.sea.children.length; i++) {
            for (var j = 0; j < this.sea.children[i].children.length; j++) {
                this.sea.children[i].children[j].rotation.x += 0.01;
            }
        }
    }

    update() {
        this.seaLoopAnimation(this.sea);

        if (this.sea.children[0].children[0].rotation.x <= 0) {
            // console.log("rising");
            this.waveRise();
        } else {
            // console.log("not rising");
        }

    }

}
