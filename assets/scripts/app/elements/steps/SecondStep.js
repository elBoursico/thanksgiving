class SecondStep extends Step {

    constructor(lib) {
        super(lib);

        this.createAnimatedMesh("testAnimation.json");
        this.lib.animatedMesh["testAnimation.json"].scale.set(30,30,30);
        this.add(this.lib.animatedMesh["testAnimation.json"]);

        // this.animations.actions["testAnimation.json-action2"].play();
        this.animations.actions["testAnimation.json-action1"].play();

    }

    update(time) {

        this.updateAnimationMixer(time);

    }

}
