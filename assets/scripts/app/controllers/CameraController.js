class CameraController extends THREE.PerspectiveCamera {

    constructor({
        fov = 60,
        aspectRatio = window.innerWidth / window.innerHeight,
        closestVisible = 0.1,
        fartherVisible = 12000
    }) {
        super(
            fov,
            aspectRatio,
            closestVisible,
            fartherVisible
        );

        this.position.set(0, 25, 100);

    }

    update(mouse) {

        let newMouse = mouse.clone();
        newMouse.y = (newMouse.y + 1) / 2;
        // this.position.x += ( newMouse.x * 30 - this.position.x ) * .5;
        this.position.x += ( newMouse.x * 100 - this.position.x ) * .5;
		this.position.y += (  newMouse.y * 50 - this.position.y ) * .5;

        this.lookAt(new THREE.Vector3(0,0,0));

    }



}
