class AssetsLoader {

    constructor() {

        const manager = new THREE.LoadingManager();
        manager.onLoad = this.onLoad.bind(this);
        this.jsonLoader = new THREE.JSONLoader(manager);
        this.objectLoader = new THREE.ObjectLoader(manager);
        this.fontLoader = new THREE.FontLoader(manager);

        this.state = "start";
        this.lib = {
            geos : {},
            mats : {
                standard : new THREE.MeshPhongMaterial({color : 0xFFFFFF}),
            },
            anims : {},
            fonts : {}
        };

        this.originObj = "../obj/";
        this.originFont = "../fonts/";

        this.loadElement('testWave.json');
        this.loadElement("livre.js");
        this.loadElement("oiseau.js");
        this.loadElement("LIVRE_QUAD.js");
        this.loadElement("LIVRE_TRIS.js");
        this.loadElement("phare.json");
        this.loadElement("testAnimation.json");
        this.loadFont("helvetiker_regular.typeface.json");

        // TODO: ameliorer le systeme de loader, pour que je puisse load tout les assets d'un fichier grace à cela, plus one by one

    }

    onLoad() {
        console.log(this.lib, "lib loaded");
        this.state = "loaded";
    }

    loadElement(nameString) {

        this.jsonLoader.load(
            this.originObj + nameString,
            (geometry, materials) => {

                this.lib.geos[nameString] = geometry;

                if (materials) {
                    // materials[0].skinning = true;
                    this.lib.mats[nameString] = materials[0];
                }
                // TODO: changer le systeme de gestion des matérials si ce loader renvoie toujours un tableau et que il n'y a chaque fois qu'un seul material;
            }
        );

    }

    // loadObjectElement(nameString) {
    //     this.objectLoader.load(
    //         this.originObj + nameString,
    //         (object) => {
    //
    //             let mesh;
    //
    //             console.log(object);
    //             object.traverse( function ( child ) {
    //
	// 				if ( child instanceof THREE.SkinnedMesh ) {
	// 					mesh = child;
	// 				}
    //
	// 			} );
    //
	// 			if ( mesh === undefined ) {
    //
	// 				alert( 'Unable to find a SkinnedMesh in this place' );
	// 				return;
    //
	// 			}
    //         }
    //     );
    // }

    loadFont(nameString) {

        this.fontLoader.load(
            this.originFont + nameString,
            (font) => {
                this.lib.fonts[nameString] = font;
            }
        );

    }

}
