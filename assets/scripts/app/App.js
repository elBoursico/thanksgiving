class App {

    constructor(lib) {

        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.lib = lib;

        // setup three
        this.renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });

        this.white = 0xFFFFFF;
        this.black = 0x000000;

        this.clock = new THREE.Clock();

        // setup renderer
        this.canvasHolder = document.body;
        this.renderer.setSize(this.width, this.height);
        this.renderer.domElement.style.position = "fixed";
        this.renderer.domElement.style.top = "0";
        this.renderer.domElement.style.left = "0";
        this.renderer.domElement.style.zIndex = "0";
        this.renderer.domElement.classList.add("canvasTHREE");
        this.renderer.localClippingEnabled = true;
        this.renderer.shadowMap.enabled = true;
        // this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        this.renderer.shadowMap.type = THREE.PCFShadowMap;
        this.canvasHolder.appendChild(this.renderer.domElement);
        this.scene = new THREE.Scene();

        // setup camera
        this.camera = new CameraController(
            60,
            window.innerWidth / window.innerHeight,
            0.1,
            12000
        );

        // Create scene controller
        // this.animationsController = new AnimationsController(this.lib);
        this.sceneController = new SceneController(this.lib);
        this.scene.add(this.sceneController);

        // this.rain = new Rain();
        // this.scene.add(this.rain);

        this.mouse = new THREE.Vector2(0,0);
        this.raycaster = new THREE.Raycaster();
        const geoHelper = new THREE.BoxGeometry( 1, 1, 1);
        this.rayHelper = new THREE.Mesh(geoHelper, new THREE.MeshBasicMaterial({color : 0xff0000}));
        this.scene.add(this.rayHelper);

        this.stats = new Stats();
		this.canvasHolder.appendChild( this.stats.dom );

        // about the mouse
        window.addEventListener("mousemove", this.onMouseMove.bind(this));

        // about the resize
        window.addEventListener( 'resize', this.onWindowResize.bind(this), false );

    }
    onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize( window.innerWidth, window.innerHeight);
    };

    onMouseMove(event) {
        this.mouse.x = ((event.pageX / window.innerWidth) * 2) - 1;
        this.mouse.y = - ((event.pageY / window.innerHeight) * 2) + 1;
    }

    raycasting(raycaster, mouse, camera) {

        raycaster.setFromCamera( mouse, camera );

        let intersects = raycaster.intersectObjects(this.scene.children[0].children, true);

        if (intersects.length != 0) {
            this.rayHelper.position.set(0,0,0);
            this.rayHelper.lookAt(intersects[0].face.normal);
            this.rayHelper.position.copy(intersects[0].point);
        }

    }

    update () {

        let delta = this.clock.getDelta();

        this.stats.begin();

        // this.mixer.update(this.clock.getDelta());
        // console.log(this.clock.getDelta());
        // this.rain.update();
        this.sceneController.update(delta);
        this.camera.update(this.mouse);
        App.render.call(this);
        this.raycasting(this.raycaster, this.mouse, this.camera);
        // if some children have their own animation
        // this.scene.traverse((child)=>{
        //     if (child.update) {
        //         child.update();
        //     }
        // });
        this.stats.end();
    }

    static render(){
        this.renderer.setClearColor(this.black, 0);
        this.renderer.render(this.scene, this.camera);
    }

}
